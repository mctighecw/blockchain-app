#!/bin/sh

echo Registering additional nodes...

curl -X POST \
  http://127.0.0.1:7100/api/bc/register_with \
  -H 'Content-Type: application/json' \
  -d '{"main_node": "http://backend:7000/", "new_node_address": "http://backend-node1:7100/"}'

curl -X POST \
  http://127.0.0.1:7200/api/bc/register_with \
  -H 'Content-Type: application/json' \
  -d '{"main_node": "http://backend:7000/", "new_node_address": "http://backend-node2:7200/"}'
