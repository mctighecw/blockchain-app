from src.app import create_app
from src.env_config import FLASK_ENV, APP_PORT

if __name__ == "__main__":
    app = create_app()
    mode = FLASK_ENV == "development"
    port = APP_PORT

    print(f"--- Starting blockchain server on port {port}")

    app.run(port=port, host="0.0.0.0", debug=mode)
