import logging

from flask import Flask
from flask_cors import CORS
from flask_mongoengine import MongoEngine

from src import flask_config
from src.env_config import FLASK_ENV

from src.routes.user import user
from src.routes.bc import bc
from src.routes.account import account

def create_app():
    app = Flask(__name__)
    app.config.from_object(flask_config)

    if FLASK_ENV == "development":
        CORS(app)

    logging.basicConfig(format="%(asctime)s %(levelname)s [%(module)s %(lineno)d] %(message)s",
                        level=app.config["LOG_LEVEL"])

    db = MongoEngine(app)

    app.register_blueprint(user, url_prefix="/api/user")
    app.register_blueprint(bc, url_prefix="/api/bc")
    app.register_blueprint(account, url_prefix="/api/account")

    return app
