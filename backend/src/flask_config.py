import logging

from src.env_config import (FLASK_ENV, DB_HOST,
    DB_AUTH, DB_NAME, DB_USER, DB_PASSWORD)


if FLASK_ENV == "development":
    LOG_LEVEL = logging.DEBUG
else:
    LOG_LEVEL = logging.INFO

MONGODB_SETTINGS = {
    'db': DB_NAME,
    'username': DB_USER,
    'password': DB_PASSWORD,
    'host': DB_HOST,
    'authentication_source': DB_AUTH
}
