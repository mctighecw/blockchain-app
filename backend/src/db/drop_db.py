import os
import sys

root_dir = os.path.abspath(os.curdir)
sys.path.append(root_dir)

import pymongo
from src.env_config import (DB_HOST, DB_AUTH,
    DB_NAME, DB_USER, DB_PASSWORD)

client = pymongo.MongoClient(
    DB_HOST,
    port=27017,
    username=DB_USER,
    password=DB_PASSWORD,
    authSource=DB_AUTH)

print('Dropping database collections...')

db = client.get_database(DB_NAME)
users = db.users
users.drop()
