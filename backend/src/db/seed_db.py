import os
import sys

root_dir = os.path.abspath(os.curdir)
sys.path.append(root_dir)

from flask_bcrypt import Bcrypt
from models import User
from seeds import users_data

bcrypt = Bcrypt()

def seed_db():
    """
    Seeds database with initial data.
    """
    for item in users_data:
        print(item["email"])
        password_hash = bcrypt.generate_password_hash(item["password"]).decode("utf-8")
        new_user = User(
            email=item["email"],
            password=password_hash,
            telegram_id=item["telegram_id"],
            admin=item["admin"]
        )
        new_user.save()
