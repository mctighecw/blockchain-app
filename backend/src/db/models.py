from datetime import datetime, timezone

from mongoengine import Document
from mongoengine.fields import StringField, IntField, BooleanField, DateTimeField


class User(Document):
    meta = { 'collection': 'users' }

    email = StringField()
    password = StringField()
    telegram_id = IntField(default=0)
    admin = BooleanField(default=False)
    registered_at = DateTimeField(default=datetime.now(timezone.utc).astimezone())
