from src.blockchain.classes import Blockchain

# The node's copy of blockchain
blockchain = Blockchain()
blockchain.create_genesis_block()
