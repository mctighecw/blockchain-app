import requests
import json

from src.blockchain.classes import Block, Blockchain
from src.blockchain.instance import blockchain
from src.blockchain.peers import peers
from src.blockchain.utils import format_bc_url
from src.constants import json_headers


def mine_unconfirmed_transactions():
    """
    Request the node to mine the unconfirmed
    transactions, if any.
    """
    result = blockchain.mine()

    if not result:
        print("No transactions to mine")
    else:
        # Make sure we have the longest chain before announcing to the network
        chain_length = len(blockchain.chain)
        consensus()

        if chain_length == len(blockchain.chain):
            # Announce the recently mined block to the network
            announce_new_block(blockchain.last_block)

        block_index = blockchain.last_block.index
        return f"Block #{block_index} has been mined"


def create_chain_from_dump(chain_dump):
    generated_blockchain = Blockchain()
    generated_blockchain.create_genesis_block()

    for idx, block_data in enumerate(chain_dump):
        if idx == 0:
            # Skip genesis block
            continue
        block = Block(block_data["index"],
                      block_data["transactions"],
                      block_data["timestamp"],
                      block_data["previous_hash"],
                      block_data["nonce"])
        proof = block_data['hash']
        added = generated_blockchain.add_block(block, proof)

        if not added:
            raise Exception("The chain dump has been tampered with!")

    return generated_blockchain


def consensus():
    """
    A simple consensus algorithm: if a longer valid chain is
    found, our chain is replaced with it.
    """
    global blockchain

    longest_chain = None
    current_len = len(blockchain.chain)

    for node in peers:
        url = format_bc_url(node, "chain")
        response = requests.get(url)
        length = response.json()['length']
        chain = response.json()['chain']

        if length > current_len and blockchain.check_chain_validity(chain):
            current_len = length
            longest_chain = chain

    if longest_chain:
        blockchain = longest_chain
        return True

    return False


def announce_new_block(block):
    """
    A function to announce to the network once a block has been mined.
    Other blocks can simply verify the proof of work and add it to their
    respective chains.
    """
    for peer in peers:
        url = format_bc_url(peer, "add_block")

        requests.post(url,
                      data=json.dumps(block.__dict__, sort_keys=True),
                      headers=json_headers)
