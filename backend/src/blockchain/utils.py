import datetime

def format_bc_url(base_url, endpoint):
    url = f"{base_url}api/bc/{endpoint}"
    return url

def format_date_time(timestamp):
    """
    Formats a numerical timestamp to `day month year, hour:minute`
    (01 Aug 2020, 12:30) string format.
    """
    format = "%d %b %Y, %H:%M"
    return datetime.datetime.fromtimestamp(timestamp).strftime(format)

def timestamp_to_utc(timestamp):
    """
    Formats a numerical timestamp to UTC format.
    """
    return datetime.datetime.utcfromtimestamp(timestamp)

def round_two_decimals(number):
    """
    Formats a float to a string with two decimal places.
    """
    return "{:.2f}".format(number)
