import os

# App
FLASK_ENV = os.getenv("FLASK_ENV")
APP_PORT = os.getenv("APP_PORT")

# Database
DB_HOST = os.getenv("DB_HOST")
DB_AUTH = os.getenv("DB_AUTH")
DB_NAME = os.getenv("DB_NAME")
DB_USER = os.getenv("DB_USER")
DB_PASSWORD = os.getenv("DB_PASSWORD")

# Telegram Bot
TELEGRAM_BOT_TOKEN = os.getenv("TELEGRAM_BOT_TOKEN")
