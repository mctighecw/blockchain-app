import telegram
from src.env_config import TELEGRAM_BOT_TOKEN

bot = telegram.Bot(token=TELEGRAM_BOT_TOKEN)

def get_bot_info():
    bot_info = bot.get_me()
    print(bot_info)

def send_message(id, text):
    bot.send_message(chat_id=id, text=text)
