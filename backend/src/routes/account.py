from flask import request, Blueprint, jsonify

from src.blockchain.instance import blockchain
from src.db.models import User as UserModel
from src.constants import default_headers

account = Blueprint("account", __name__)


@account.route("/user", methods=["POST"])
def user_account():
    """
    Retrieves an account summary for a single user.
    """
    req = request.json
    email = req["email"].lower()

    user = UserModel.objects.filter(email=email).first()

    if user is None:
        res = {"status": "Error", "message": "User not found"}
        return jsonify(res), 400, default_headers
    else:
        sent_amount = 0
        received_amount = 0
        number_transactions = 0

        for block in blockchain.chain:
            for transaction in block.__dict__["transactions"]:

                if transaction["sender"] == email:
                    sent_amount += transaction["amount"]
                    number_transactions += 1

                if transaction["recipient"] == email:
                    received_amount += transaction["amount"]
                    number_transactions += 1

        account = {
            "email": email,
            "sent_amount": sent_amount,
            "received_amount": received_amount,
            "number_transactions": number_transactions,
        }

        res = {"status": "OK", "message": "User account retrieved successfully", "account": account}
        return jsonify(res), 200, default_headers


@account.route("/recipient", methods=["POST"])
def recipient_account():
    """
    Retrieves a summary of funds received for a single user.
    """
    req = request.json
    email = req["email"].lower()

    user = UserModel.objects.filter(email=email).first()

    if user is None:
        res = {"status": "Error", "message": "User not found"}
        return jsonify(res), 400, default_headers
    else:
        received_amount = 0
        received_transactions = 0
        senders = []

        for block in blockchain.chain:
            for transaction in block.__dict__["transactions"]:

                if transaction["recipient"] == email:
                    sender = transaction["sender"]
                    received_amount += transaction["amount"]
                    received_transactions += 1

                    if sender not in senders:
                        senders.append(sender)

        account = {
            "email": email,
            "received_amount": received_amount,
            "received_transactions": received_transactions,
            "senders": senders,
        }

        res = {"status": "OK", "message": "Recipient data retrieved successfully", "account": account}
        return jsonify(res), 200, default_headers


@account.route("/sender", methods=["POST"])
def sender_account():
    """
    Retrieves a summary of funds sent for a single user.
    """
    req = request.json
    email = req["email"].lower()

    user = UserModel.objects.filter(email=email).first()

    if user is None:
        res = {"status": "Error", "message": "User not found"}
        return jsonify(res), 400, default_headers
    else:
        sent_amount = 0
        sent_transactions = 0
        recipients = []

        for block in blockchain.chain:
            for transaction in block.__dict__["transactions"]:

                if transaction["sender"] == email:
                    recipient = transaction["recipient"]
                    sent_amount += transaction["amount"]
                    sent_transactions += 1

                    if recipient not in recipients:
                        recipients.append(recipient)

        account = {
            "email": email,
            "sent_amount": sent_amount,
            "sent_transactions": sent_transactions,
            "recipients": recipients,
        }

        res = {"status": "OK", "message": "Sender data retrieved successfully", "account": account}
        return jsonify(res), 200, default_headers


@account.route("/calculate", methods=["POST"])
def calculate_accounts():
    """
    Retrieves a summary for two users and calculates the net amount.
    """
    req = request.json

    if not all(k in req for k in ["email1", "email2"]):
        res = {"status": "Error", "message": "Please provide all required fields"}
        return jsonify(res), 400, default_headers
    else:
        email1 = req["email1"].lower()
        email2 = req["email2"].lower()

        user1 = UserModel.objects.filter(email=email1).first()
        user2 = UserModel.objects.filter(email=email2).first()

        if user1 is None or user2 is None:
            res = {"status": "Error", "message": "User(s) not found"}
            return jsonify(res), 400, default_headers
        else:
            user1_to_user2 = 0
            user2_to_user1 = 0
            user1_to_user2_tx = 0
            user2_to_user1_tx = 0

            for block in blockchain.chain:
                for transaction in block.__dict__["transactions"]:

                    if transaction["sender"] == email1 and transaction["recipient"] == email2:
                        user1_to_user2 += transaction["amount"]
                        user1_to_user2_tx += 1

                    if transaction["sender"] == email2 and transaction["recipient"] == email1:
                        user2_to_user1 += transaction["amount"]
                        user2_to_user1_tx += 1

            if user1_to_user2 == user2_to_user1:
                summary = f"{email1} and {email2} break even"
            else:
                if user1_to_user2 > user2_to_user1:
                    creditor = email2
                    debtor = email1
                    difference = user1_to_user2 - user2_to_user1
                elif user1_to_user2 < user2_to_user1:
                    creditor = email1
                    debtor = email2
                    difference = user2_to_user1 - user1_to_user2

                difference = format(difference, ".2f")
                summary = f"{debtor} has paid {creditor} net ${difference}"

            calculation = {
                "users": {
                    "user1": email1,
                    "user2": email2,
                },
                "user1_to_user2": {
                    "amount_sent": user1_to_user2,
                    "number_transactions": user1_to_user2_tx,
                },
                "user2_to_user1": {
                    "amount_sent": user2_to_user1,
                    "number_transactions": user2_to_user1_tx,
                },
                "summary": summary,
            }

            res = {"status": "OK", "message": "Accounts data calculated successfully", "calculation": calculation}
            return jsonify(res), 200, default_headers
