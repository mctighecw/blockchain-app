from flask import request, Blueprint, jsonify
from flask_bcrypt import Bcrypt

from src.db.models import User as UserModel
from src.constants import default_headers

bcrypt = Bcrypt()

user = Blueprint("user", __name__)


@user.route("/register", methods=["POST"])
def register():
    """
    Registers a new user.
    """
    req = request.json

    if all(k in req for k in ["email", "password"]):
        email = req["email"].lower()
        password = req["password"]

        exist_email = UserModel.objects.filter(email=email).first()

        if exist_email:
            res = {"status": "Error", "message": "Email address is already taken"}
            return jsonify(res), 409, default_headers

        else:
            password_hash = bcrypt.generate_password_hash(password).decode("utf-8")
            telegram_id = req["telegram_id"] if "telegram_id" in req else 0

            new_user = UserModel(
                email=email,
                password=password_hash,
                telegram_id=telegram_id
            )
            new_user.save()

            res = {"status": "OK", "message": "User has successfully registered"}
            return jsonify(res), 200, default_headers
    else:
        res = {"status": "Error", "message": "Incomplete request"}
        return jsonify(res), 400, default_headers


@user.route("/delete", methods=["POST"])
def delete():
    """
    Allows an admin to delete a user from database.
    """
    req = request.json

    if all(k in req for k in ["email", "password", "id"]):
        email = req["email"].lower()
        password = req["password"]
        id = req["id"]

        admin_user = UserModel.objects.filter(email=email).first()

        if admin_user is not None and \
            admin_user.admin and \
            bcrypt.check_password_hash(admin_user.password, password):
            user = UserModel.objects(id=id)

            if len(user) > 0:
                user.delete()
                res = {"status": "OK", "message": "User has been deleted"}
                return jsonify(res), 200, default_headers
            else:
                res = {"status": "Error", "message": "No user with that ID found"}
                return jsonify(res), 400, default_headers

        else:
            res = {"status": "Error", "message": "Forbidden"}
            return jsonify(res), 403, default_headers

    else:
        res = {"status": "Error", "message": "Incomplete request"}
        return jsonify(res), 400, default_headers


@user.route("/all", methods=["POST"])
def user_all():
    """
    Allows an admin to get a list of all users in database.
    """
    req = request.json

    if all(k in req for k in ["email", "password"]):
        email = req["email"].lower()
        password = req["password"]

        admin_user = UserModel.objects.filter(email=email).first()

        if admin_user is not None and \
            admin_user.admin and \
            bcrypt.check_password_hash(admin_user.password, password):

            all_users = UserModel.objects.all()
            users_list = []

            for u in all_users:
                id = str(u.id)
                users_list.append({
                    "id": id,
                    "email": u.email,
                    "telegram_id": u.telegram_id,
                    "admin": u.admin,
                    "registered_at": u.registered_at
                })

            res = {"status": "OK", "users": users_list}
            return jsonify(res), 200, default_headers

        else:
            res = {"status": "Error", "message": "Forbidden"}
            return jsonify(res), 403, default_headers

    else:
        res = {"status": "Error", "message": "Incomplete request"}
        return jsonify(res), 400, default_headers


@user.route("/emails", methods=["GET"])
def user_emails():
    """
    Gets a list of all user emails in database.
    """
    all_users = UserModel.objects.all()
    users_list = [ u.email for u in all_users ]

    res = {"status": "OK", "users": users_list}
    return jsonify(res), 200, default_headers
