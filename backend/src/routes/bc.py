import json
import time

from flask import Blueprint, request, jsonify
from flask_bcrypt import Bcrypt
import requests

from src.blockchain.classes import Block
from src.blockchain.instance import blockchain
from src.blockchain.functions import (mine_unconfirmed_transactions, create_chain_from_dump,
    consensus, announce_new_block)
from src.blockchain.peers import peers
from src.constants import default_headers, json_headers
from src.blockchain.utils import timestamp_to_utc, format_bc_url
from src.blockchain.utils import round_two_decimals

from src.telegram_bot import send_message
from src.db.models import User as UserModel

bcrypt = Bcrypt()

bc = Blueprint("bc", __name__)


#################################
# General blockchain endpoints  #
#################################

@bc.route("/new_transaction", methods=["POST"])
def new_transaction():
    """
    Endpoint to submit a new transaction, to add new data
    to the blockchain.
    """
    req = request.json

    req_fields = ["sender", "recipient", "amount"]
    req_subfields = ["email", "password"]

    if all(k in req for k in req_fields) and \
        all(k in req["sender"] for k in req_subfields):
        sender_email = req["sender"]["email"]
        sender_password = req["sender"]["password"]

        sender_user = UserModel.objects.filter(email=sender_email).first()

        if sender_user is not None and \
            bcrypt.check_password_hash(sender_user.password, sender_password):
            recipient = req["recipient"]
            amount = req["amount"]
            telegram_id = sender_user.telegram_id

            tx_data = {
                "sender": sender_email,
                "recipient": recipient,
                "amount": amount,
                "timestamp": time.time()
            }

            blockchain.add_new_transaction(tx_data)
            mine_unconfirmed_transactions()

            if telegram_id != 0:
                telegram_text = (f"Blockchain App new transaction: ${round_two_decimals(amount)} " +
                    f"has been sent from {sender_email} to {recipient}")
                send_message(telegram_id, telegram_text)

            res = {"status": "OK", "message": "New transaction successful"}
            return jsonify(res), 200, default_headers

        else:
            res = {"status": "Error", "message": "Invalid sender or password"}
            return jsonify(res), 401, default_headers

    else:
        res = {"status": "Error", "message": "Invalid transaction data"}
        return jsonify(res), 400, default_headers


@bc.route("/chain", methods=["GET"])
def get_chain():
    """
    Returns all chain data and transactions.
    """
    chain_data = []

    for block in blockchain.chain:
        chain_data.append(block.__dict__)

    res = {
        "length": len(chain_data),
        "chain": chain_data,
        "peers": list(peers)}

    return jsonify(res), 200, default_headers


@bc.route("/transactions", methods=["GET"])
def get_transactions():
    """
    Returns all transaction data with formatted timestamp.
    """
    transactions_data = []
    index = 0

    for block in blockchain.chain:
        for transaction in block.__dict__["transactions"]:
            date_time = timestamp_to_utc(transaction["timestamp"])
            transaction_dict = {
                "sender": transaction["sender"],
                "recipient": transaction["recipient"],
                "amount": transaction["amount"],
                "key": index,
                "date_time": date_time}
            transactions_data.append(transaction_dict)
            index += 1

    res = {"status": "OK", "transactions": transactions_data, "length": len(transactions_data)}
    return jsonify(res), 200, default_headers


@bc.route("/add_block", methods=["POST"])
def verify_and_add_block():
    """
    Endpoint to add a block mined by someone else to
    the node's chain. The block is first verified by the node
    and then added to the chain.
    """
    block_data = request.get_json()
    block = Block(block_data["index"],
                  block_data["transactions"],
                  block_data["timestamp"],
                  block_data["previous_hash"],
                  block_data["nonce"])

    proof = block_data["hash"]
    added = blockchain.add_block(block, proof)

    if not added:
        res = {"status": "Error", "message": "The block was discarded by the node"}
        return jsonify(res), 400, default_headers
    else:
        res = {"status": "OK", "message": "Block added to the chain"}
        return jsonify(res), 200, default_headers


@bc.route("/pending_tx", methods=["GET"])
def get_pending_tx():
    """
    Endpoint to query unconfirmed transactions.
    """
    res = {"status": "OK", "pending_tx": blockchain.unconfirmed_transactions}
    return jsonify(res), 200, default_headers


#########################################
# Endpoints to add new nodes to network #
#########################################

@bc.route("/register_with", methods=["POST"])
def register_with_existing_node():
    """
    Internally calls the `register_node` endpoint to
    register current node with the node specified in the
    request, and sync the blockchain and peer data.
    """
    data = request.get_json()
    main_node = data["main_node"]
    new_node_address = data["new_node_address"]

    if not main_node:
        res = {"status": "Error", "message": "Missing main node address"}
        return jsonify(res), 400, default_headers
    else:
        post_url = format_bc_url(main_node, "register_node")
        req_data = {"new_node_address": new_node_address}

        response = requests.post(post_url,
                                 data=json.dumps(req_data),
                                 headers=json_headers)

        if response.status_code == 200:
            global blockchain
            global peers

            # Update chain and peers
            chain_dump = response.json()["chain"]
            blockchain = create_chain_from_dump(chain_dump)
            peers.update(response.json()["peers"])

            node_url = request.url.split("/api")
            node_url = node_url[0]
            message = f"{node_url} has been registered"
            res = {"status": "OK", "message": message}
            return jsonify(res), 200, default_headers
        else:
            res = {"status": "Error", "message": response.content}
            return jsonify(res), response.status_code, default_headers


@bc.route("/register_node", methods=["POST"])
def register_new_peers():
    """
    Endpoint to add new peers to the network, used by
    `register_with` request.
    """
    data = request.get_json()
    new_node_address = data["new_node_address"]

    # Add node to the peer list
    peers.add(new_node_address)

    # Return the consensus blockchain to the new node
    return get_chain()
