# README

A basic blockchain app where users can submit information that is added to a blockchain sequence. Multiple network nodes, which are synchronized with the shared blockchain, can also be added.

The app uses a React frontend and a Python Flask backend.

Since I'm new to creating a blockchain from scratch, I used this useful [tutorial](https://developer.ibm.com/technologies/blockchain/tutorials/develop-a-blockchain-application-from-scratch-in-python) to get familiar with the basic concepts and code. The full code is available at this [GitHub repository](https://github.com/satwikkansal/python_blockchain_app/tree/ibm_blockchain_post).

_Note_: I adopted much of the backend Python code for creating and maintaining the blockchain, but also made some changes and reorganized the code structure in my app.

## App Information

App Name: blockchain-app

Created: August 2020

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/blockchain-app)

## Tech Stack

- React
- Ant Design (Less)
- Sass & CSS Modules
- Prettier
- Python
- Flask
- MongoDB
- Docker & NGINX
- Telegram Bot

## Create a Network with Multiple Nodes

There is a main blockchain network node, but it is possible to register new nodes on the network, which will sync with the shared blockchain.

This requires the following steps:

1. Start running the network node servers (if running on _localhost_, using different ports)
2. Use the `register_with` POST request to add them to the network

   _Note_: If running them within the same Docker network, the Docker container address is required

## To Setup and Run (with Docker)

Start the Docker network:

```
$ docker-compose -f docker-compose-dev.yml up -d --build
```

Set up database:

```
$ docker exec -it blockchain-app_mongodb_1 bash
  (set up admin user and password)
$ source init_docker_db.sh
```

Register two extra network nodes:

```
$ source register_nodes.sh
```

The project is now running in development mode at `http://localhost:3000` with three backend network nodes (on ports 7000, 7100, 7200).

_Note_: When all nodes stop running, the blockchain and its data are lost.

## Credits

Blockchain icon courtesy of [Good Ware](https://www.flaticon.com/authors/good-ware)

Last updated: 2024-07-30
