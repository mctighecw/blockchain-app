#!/bin/sh

echo Initializing database in container...

docker exec -it blockchain-app_backend_1 python ./src/db/init_db.py
