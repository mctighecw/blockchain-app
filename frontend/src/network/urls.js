export const getUrl = (cateogry, type) => {
  const dev = process.env.NODE_ENV === 'development';
  const baseUrl = dev ? 'http://localhost:7000' : '';
  return `${baseUrl}/api/${cateogry}/${type}`;
};
