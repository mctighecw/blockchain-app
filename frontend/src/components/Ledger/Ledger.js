import React, { useState } from 'react';
import { Tabs, Divider } from 'antd';
import NewTransaction from 'Components/NewTransaction/NewTransaction';
import AllTransactions from 'Components/AllTransactions/AllTransactions';
import UserTransactions from 'Components/UserTransactions/UserTransactions';

import BlockchainIcon from 'Assets/blockchain-icon.svg';
import './styles.scss';

const Ledger = () => {
  const [timestamp, setTimestamp] = useState(new Date());

  const setNewTimestamp = (ts) => {
    setRefreshTimestamp(ts);
  };

  return (
    <div styleName="container">
      <div styleName="heading">
        <img src={BlockchainIcon} alt="Logo" />
        <div styleName="title">Payment Ledger</div>
      </div>

      <NewTransaction setTimestamp={setTimestamp} />
      <Divider />

      <Tabs defaultActiveKey="1">
        <Tabs.TabPane tab="All Transactions" key="1">
          <AllTransactions timestamp={timestamp} />
        </Tabs.TabPane>
        <Tabs.TabPane tab="User transactions" key="2">
          <UserTransactions timestamp={timestamp} />
        </Tabs.TabPane>
      </Tabs>
    </div>
  );
};

export default Ledger;
