import React, { useState, useEffect } from 'react';
import { get, post } from 'Network/requests';
import { getUrl } from 'Network/urls';
import { Select, Table } from 'antd';
import { numberWithCommas } from 'Utils/functions';

import 'Stylesheets/table-styles.less';
import './styles.scss';

const UserTransactions = ({ timestamp }) => {
  const [user, setUser] = useState('');
  const [users, setUsers] = useState([]);
  const [data, setData] = useState([]);

  const onSelectUser = (value) => {
    setUser(value);
    getUserTransactions(value);
  };

  const getUsersInfo = () => {
    const url = getUrl('user', 'emails');

    get(url)
      .then((res) => {
        const { users } = res.data;
        setUsers(users);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const getUserTransactions = (email) => {
    const url = getUrl('account', 'user');
    const data = { email };

    post(url, data)
      .then((res) => {
        const { account } = res.data;
        account['key'] = 0;
        setData([account]);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    getUsersInfo();
    if (user !== '') getUserTransactions(user);
  }, [timestamp]);

  const columns = [
    {
      title: 'Number of transactions',
      dataIndex: 'number_transactions',
      key: 'number_transactions',
      align: 'center',
      className: 'column-center',
    },
    {
      title: 'Sent amount',
      key: 'sent_amount',
      align: 'right',
      className: 'column-right',
      sorter: (a, b) => a.sent_amount - b.sent_amount,
      render: (record) => {
        const amount = numberWithCommas(record.sent_amount);
        return `$ ${amount}`;
      },
    },
    {
      title: 'Received amount',
      key: 'received_amount',
      align: 'right',
      className: 'column-right',
      sorter: (a, b) => a.received_amount - b.received_amount,
      render: (record) => {
        const amount = numberWithCommas(record.received_amount);
        return `$ ${amount}`;
      },
    },
  ];

  const userOptions = users.map((item, index) => (
    <Select.Option key={index} value={item}>
      {item}
    </Select.Option>
  ));

  return (
    <div styleName="container">
      <div styleName="label">Select a user:</div>

      <Select defaultValue="" style={{ width: 200, marginBottom: 20 }} onChange={onSelectUser}>
        {userOptions}
      </Select>

      {data.length > 0 && <Table dataSource={data} columns={columns} pagination={false} />}
    </div>
  );
};

export default UserTransactions;
