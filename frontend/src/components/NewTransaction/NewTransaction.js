import React, { useState } from 'react';
import { post } from 'Network/requests';
import { getUrl } from 'Network/urls';
import { Input, Button, notification } from 'antd';
import { formatCurrencyValue } from 'Utils/functions';

import './styles.scss';

const NewTransaction = ({ setTimestamp }) => {
  const [sender, setSender] = useState('john@smith.com');
  const [senderPassword, setSenderPassword] = useState('john1');
  const [recipient, setRecipient] = useState('susan@williams.com');
  const [amount, setAmount] = useState(10);

  const onChangeAmount = (e) => {
    const amount = formatCurrencyValue(e.target.value);
    setAmount(amount);
  };

  const clearFields = () => {
    setSender('');
    setSenderPassword('');
    setRecipient('');
    setAmount('');
  };

  const checkFields = (fieldsArray) => {
    const emptyFields = fieldsArray.filter((item) => item === '');
    const valid = emptyFields.length === 0;
    return valid;
  };

  const postTransaction = () => {
    const valid = checkFields([sender, senderPassword, recipient, amount]);

    if (valid) {
      const url = getUrl('bc', 'new_transaction');
      const data = {
        sender: {
          email: sender,
          password: senderPassword,
        },
        recipient,
        amount: +amount,
      };

      post(url, data)
        .then((res) => {
          clearFields();
          setTimestamp(new Date());
          notification['success']({
            message: 'Success',
            description: 'Transaction posted',
          });
        })
        .catch((err) => {
          console.log(err);
          let errorMessage = 'Problem posting transaction';

          if (err.response && err.response.data && err.response.data.message) {
            errorMessage = err.response.data.message;
          }
          notification['error']({
            message: 'Error',
            description: errorMessage,
          });
        });
    } else {
      notification['error']({
        message: 'Error',
        description: 'All fields are required',
      });
    }
  };

  return (
    <div styleName="container">
      <div styleName="box">
        <div styleName="label">New transaction</div>
        <div styleName="row">
          <Input
            value={sender}
            placeholder="Sender email"
            maxLength={20}
            style={{ marginRight: 10 }}
            onChange={(e) => setSender(e.target.value)}
          />

          <Input.Password
            value={senderPassword}
            placeholder="Sender password"
            maxLength={20}
            onChange={(e) => setSenderPassword(e.target.value)}
          />
        </div>

        <div styleName="row">
          <Input
            value={recipient}
            placeholder="Recipient email"
            maxLength={20}
            style={{ marginRight: 10 }}
            onChange={(e) => setRecipient(e.target.value)}
          />

          <Input
            value={amount}
            prefix="$"
            placeholder="Amount"
            maxLength={20}
            style={{ width: 200 }}
            onChange={onChangeAmount}
          />
        </div>

        <Button type="primary" style={{ width: 200 }} onClick={postTransaction}>
          Submit
        </Button>
      </div>
    </div>
  );
};

export default NewTransaction;
