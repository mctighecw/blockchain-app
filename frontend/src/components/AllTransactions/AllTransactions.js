import React, { useState, useEffect } from 'react';
import { get } from 'Network/requests';
import { getUrl } from 'Network/urls';
import { Table } from 'antd';
import { formatDateTime, numberWithCommas } from 'Utils/functions';

import 'Stylesheets/table-styles.less';
import './styles.scss';

const AllTransactions = ({ timestamp }) => {
  const [data, setData] = useState([]);

  const getTransactions = () => {
    const url = getUrl('bc', 'transactions');

    get(url)
      .then((res) => {
        const { transactions } = res.data;
        setData(transactions);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    getTransactions();
  }, [timestamp]);

  const columns = [
    {
      title: 'Sender',
      dataIndex: 'sender',
      key: 'sender',
      align: 'center',
      className: 'column-center',
    },
    {
      title: 'Recipient',
      dataIndex: 'recipient',
      key: 'recipient',
      align: 'center',
      className: 'column-center',
    },
    {
      title: 'Date/Time',
      key: 'date_time',
      align: 'center',
      className: 'column-center',
      render: (record) => formatDateTime(record.date_time),
    },
    {
      title: 'Amount',
      key: 'amount',
      align: 'right',
      className: 'column-right',
      sorter: (a, b) => a.amount - b.amount,
      render: (record) => {
        const amount = numberWithCommas(record.amount);
        return `$ ${amount}`;
      },
    },
  ];

  return (
    <div styleName="container">
      <Table dataSource={data} columns={columns} pagination={false} />
    </div>
  );
};

export default AllTransactions;
