import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Ledger from 'Components/Ledger/Ledger';

import 'Stylesheets/ant-overrides.less';
import './styles/global.css';

const App = () => (
  <Router>
    <Switch>
      <Route path="/ledger" component={Ledger} />
      <Route render={() => <Redirect to="/ledger" />} />
    </Switch>
  </Router>
);

export default App;
