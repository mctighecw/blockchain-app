import moment from 'moment';

const getOffset = (datestring) => {
  const offset = moment().utcOffset();
  const currentDateTime = moment.utc(datestring).utcOffset(offset).format();
  return currentDateTime;
};

export const formatDateTime = (datestring) => {
  // Format UTC datestring to 01 Aug 2020, 14:30 format
  const currentDateTime = getOffset(datestring);
  const date = moment(currentDateTime).format('DD MMM YYYY, HH:mm');
  return date;
};

export const numberWithCommas = (number) => {
  // Format number with two decimal places, and commas for big numbers
  return number
    .toFixed(2)
    .toString()
    .replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');
};

export const formatCurrencyValue = (amount) => {
  /* Format currency value:
    - only allow numbers and a decimal
    - limit to two decimal places
  */
  let value = amount.replace(/[^0-9.]/g, '');

  if (value.includes('.')) {
    const split = value.split('.');
    value = split[0] + '.' + split[1].slice(0, 2);
  }
  return value;
};
